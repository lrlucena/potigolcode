# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table capitulo_glossario (
  id                        bigint not null,
  titulo                    varchar(255),
  conteudo                  clob,
  data_cadastro             timestamp,
  autor_id                  bigint,
  glossario_id              bigint,
  constraint uq_capitulo_glossario_titulo unique (titulo),
  constraint pk_capitulo_glossario primary key (id))
;

create table categoria_download (
  id                        integer not null,
  nome                      varchar(255),
  descricao                 clob,
  constraint uq_categoria_download_nome unique (nome),
  constraint pk_categoria_download primary key (id))
;

create table curso (
  id                        bigint not null,
  titulo                    varchar(255),
  data_cadastro             timestamp,
  autor_id                  bigint,
  constraint uq_curso_titulo unique (titulo),
  constraint pk_curso primary key (id))
;

create table download (
  id                        integer not null,
  nome                      varchar(255),
  url                       varchar(255),
  categoria_id              integer,
  constraint pk_download primary key (id))
;

create table exercicio (
  id                        bigint not null,
  titulo                    varchar(255),
  descricao                 clob,
  nivel_exercicio           integer,
  pontuacao                 integer,
  estrutura_metodo_proposto clob,
  solucao_proposta          clob,
  caso_de_teste             clob,
  curso_id                  bigint,
  autor_id                  bigint,
  constraint pk_exercicio primary key (id))
;

create table exercicio_resolvido (
  id                        integer not null,
  pontuacao_obtida          integer,
  resposta_submetida        clob,
  usuario_id                bigint,
  exercicio_id              bigint,
  constraint pk_exercicio_resolvido primary key (id))
;

create table glossario (
  id                        bigint not null,
  titulo                    varchar(255),
  descricao                 clob,
  data_cadastro             timestamp,
  autor_id                  bigint,
  constraint uq_glossario_titulo unique (titulo),
  constraint pk_glossario primary key (id))
;

create table usuario (
  id                        bigint not null,
  login                     varchar(255),
  senha                     varchar(255),
  nome                      varchar(255),
  email                     varchar(255),
  chave_redefinicao_senha   varchar(255),
  is_professor              boolean,
  is_ativo                  boolean,
  constraint uq_usuario_login unique (login),
  constraint uq_usuario_email unique (email),
  constraint pk_usuario primary key (id))
;

create sequence capitulo_glossario_seq;

create sequence categoria_download_seq;

create sequence curso_seq;

create sequence download_seq;

create sequence exercicio_seq;

create sequence exercicio_resolvido_seq;

create sequence glossario_seq;

create sequence usuario_seq;

alter table capitulo_glossario add constraint fk_capitulo_glossario_autor_1 foreign key (autor_id) references usuario (id) on delete restrict on update restrict;
create index ix_capitulo_glossario_autor_1 on capitulo_glossario (autor_id);
alter table capitulo_glossario add constraint fk_capitulo_glossario_glossari_2 foreign key (glossario_id) references glossario (id) on delete restrict on update restrict;
create index ix_capitulo_glossario_glossari_2 on capitulo_glossario (glossario_id);
alter table curso add constraint fk_curso_autor_3 foreign key (autor_id) references usuario (id) on delete restrict on update restrict;
create index ix_curso_autor_3 on curso (autor_id);
alter table download add constraint fk_download_categoria_4 foreign key (categoria_id) references categoria_download (id) on delete restrict on update restrict;
create index ix_download_categoria_4 on download (categoria_id);
alter table exercicio add constraint fk_exercicio_curso_5 foreign key (curso_id) references curso (id) on delete restrict on update restrict;
create index ix_exercicio_curso_5 on exercicio (curso_id);
alter table exercicio add constraint fk_exercicio_autor_6 foreign key (autor_id) references usuario (id) on delete restrict on update restrict;
create index ix_exercicio_autor_6 on exercicio (autor_id);
alter table exercicio_resolvido add constraint fk_exercicio_resolvido_usuario_7 foreign key (usuario_id) references usuario (id) on delete restrict on update restrict;
create index ix_exercicio_resolvido_usuario_7 on exercicio_resolvido (usuario_id);
alter table exercicio_resolvido add constraint fk_exercicio_resolvido_exercic_8 foreign key (exercicio_id) references exercicio (id) on delete restrict on update restrict;
create index ix_exercicio_resolvido_exercic_8 on exercicio_resolvido (exercicio_id);
alter table glossario add constraint fk_glossario_autor_9 foreign key (autor_id) references usuario (id) on delete restrict on update restrict;
create index ix_glossario_autor_9 on glossario (autor_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists capitulo_glossario;

drop table if exists categoria_download;

drop table if exists curso;

drop table if exists download;

drop table if exists exercicio;

drop table if exists exercicio_resolvido;

drop table if exists glossario;

drop table if exists usuario;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists capitulo_glossario_seq;

drop sequence if exists categoria_download_seq;

drop sequence if exists curso_seq;

drop sequence if exists download_seq;

drop sequence if exists exercicio_seq;

drop sequence if exists exercicio_resolvido_seq;

drop sequence if exists glossario_seq;

drop sequence if exists usuario_seq;

