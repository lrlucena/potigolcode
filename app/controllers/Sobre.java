package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.Sobre.documentacao;
import views.html.Sobre.equipePotigol;
import views.html.Sobre.equipePotigolCode;
import views.html.Sobre.historia;
import views.html.Sobre.oProjeto;

public class Sobre extends Controller {
	public static Result index() {
		return Sobre.historia();
	}
	
	public static Result historia() {
		return ok(historia.render());
	}
	
	public static Result documentacao() {
		return ok(documentacao.render());
	}
	
	public static Result equipePotigol() {
		return ok(equipePotigol.render());
	}
	
	public static Result oProjeto() {
		return ok(oProjeto.render());
	}
	
	public static Result equipePotigolCode() {
		return ok(equipePotigolCode.render());
	}
}
