package controllers;

import static controllers.routes.Cursos;
import helpers.SegurancaHelpers.InformacoesUsuarioHelper;
import helpers.SegurancaHelpers.Permissao;

import java.util.Date;
import java.util.List;

import models.Curso;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Cursos.formulario;
import views.html.Cursos.formularioEdicao;
import views.html.Cursos.index;
import views.html.Cursos.visualizar;

public class Cursos extends Controller {
	private static Form<Curso> formCurso = Form.form(Curso.class);

	public static Result index() {
		List<Curso> cursos = Curso.find.findList();
		return ok(index.render(cursos));
	}

	public static Result visualizar(Long id) {
		Curso curso = Curso.find.byId(id);
		return ok(visualizar.render(curso));
	}

	@Permissao("Professor")
	public static Result formulario() {
		return ok(formulario.render(formCurso));
	}

	@Permissao("Professor")
	public static Result cadastrar() {
		Form<Curso> form = formCurso.bindFromRequest();
		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form));
		}
		Curso curso = form.get();
		curso.autor = InformacoesUsuarioHelper.getUsuarioLogado();
		curso.dataCadastro = new Date();

		curso.save();

		flash().put(
				"success",
				"Curso <strong>\"" + curso.titulo
						+ "\"</strong> cadastrado com sucesso!");
		return redirect(Cursos.index());
	}

	public static Result formularioEdicao(Long id) {
		Curso curso = Curso.find.byId(id);
		return ok(formularioEdicao.render(formCurso.fill(curso), curso));
	}

	public static Result editar(Long id) {
		Form<Curso> form = formCurso.bindFromRequest();
		Curso curso = Curso.find.byId(id);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form.fill(form.get()),curso));
		}

		curso.setTitulo(form.get().titulo);
		curso.update();

		flash().put(
				"success",
				"Curso <strong>\"" + curso.titulo
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Cursos.index());
	}

	public static Result deletar(Long id) {
		Curso curso = Curso.find.byId(id);

		if (curso == null) {
			flash().put("error",
					"O Curso informado não foi encontrado no Sistema.");
		} else {
			curso.delete();

			flash().put(
					"success",
					"Curso <strong>\"" + curso.titulo
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Cursos.index());
	}
}
