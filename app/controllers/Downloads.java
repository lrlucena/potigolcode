package controllers;

import static controllers.routes.Downloads;
import helpers.SegurancaHelpers.Permissao;

import java.util.List;

import models.CategoriaDownload;
import models.Download;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Downloads.formulario;
import views.html.Downloads.formularioEdicao;
import views.html.Downloads.index;

public class Downloads extends Controller {
	private static Form<Download> formDownload = Form.form(Download.class);

	@Permissao("Professor")
	public static Result index() {
		return ok(index.render(Download.todos()));
	}

	@Permissao("Professor")
	public static Result formulario() {
		List<CategoriaDownload> categoriasDownload = CategoriaDownload.find
				.all();
		return ok(formulario.render(formDownload, categoriasDownload));
	}

	@Permissao("Professor")
	public static Result cadastrar() {
		Form<Download> form = formDownload.bindFromRequest();

		if (form.hasErrors()) {
			List<CategoriaDownload> categoriasDownload = CategoriaDownload.find
					.all();

			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form, categoriasDownload));
		}

		String idCategoria = form.data().get("idCategoria");
		Download download = form.get();

		download.categoria = CategoriaDownload.find.byId(Long
				.valueOf(idCategoria));
		download.save();

		flash().put(
				"success",
				"Download <strong>\"" + download.nome
						+ "\"</strong> cadastrado com sucesso!");
		return redirect(Downloads.index());
	}

	public static Result formularioEdicao(Long id) {
		Download download = Download.find.byId(id);
		List<CategoriaDownload> categoriasDownload = CategoriaDownload.find
				.all();
		return ok(formularioEdicao.render(formDownload.fill(download),categoriasDownload, download));
	}

	public static Result editar(Long id) {
		Form<Download> form = formDownload.bindFromRequest();
		Download download = Download.find.byId(id);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			List<CategoriaDownload> categoriasDownload = CategoriaDownload.find
					.all();
			return badRequest(formularioEdicao.render(form, categoriasDownload, download));
		}

		download.setNome(form.get().nome);
		download.setUrl(form.get().url);
		download.setCategoria(CategoriaDownload.find.byId(Long.valueOf(form
				.data().get("idCategoria").toString())));
		download.update();

		flash().put(
				"success",
				"Download <strong>\"" + download.nome
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Downloads.index());
	}

	@Permissao("Professor")
	public static Result deletar(Long id) {
		Download download = Download.find.byId(id);

		if (download == null) {
			flash().put("error",
					"O Download informado não foi encontrado no Sistema.");
		} else {
			download.delete();

			flash().put(
					"success",
					"Download <strong>\"" + download.nome
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Downloads.index());
	}
}
