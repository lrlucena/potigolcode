package controllers;

import static controllers.routes.Cursos;
import static controllers.routes.Exercicios;
import static emails.ApplicationMailer.enviarDuvidaExercicio;
import helpers.PotigolTestsHelper;
import helpers.SegurancaHelpers.InformacoesUsuarioHelper;
import helpers.SegurancaHelpers.Permissao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Curso;
import models.Exercicio;
import models.ExercicioResolvido;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Exercicios.formulario;
import views.html.Exercicios.formularioEdicao;
import views.html.Exercicios.index;
import views.html.Exercicios.visualizar;
import views.html.Exercicios.visualizar2;
import forms.DuvidaExercicioForm;
import forms.ExercicioForm;

public class Exercicios extends Controller {
	private static Form<Exercicio> formExer = Form.form(Exercicio.class);
	private static Form<ExercicioForm> formExercicio = Form
			.form(ExercicioForm.class);
	private static Form<DuvidaExercicioForm> formDuvida = Form
			.form(DuvidaExercicioForm.class);

	@Permissao("Professor")
	public static Result index() {
		List<Exercicio> exercicios = Exercicio.find.findList();
		return ok(index.render(exercicios));
	}

	public static Result visualizar(Long id) {
		Exercicio exercicio = Exercicio.find.byId(id);

		if (InformacoesUsuarioHelper.getUsuarioLogado().isProfessor) {
			return ok(visualizar2.render(exercicio));
		} else {
			Map<String, String> estruturaMetodo = new HashMap<>();
			estruturaMetodo.put("resposta",
					exercicio.getEstruturaMetodoProposto());

			return ok(visualizar.render(formExercicio.bind(estruturaMetodo),
					formDuvida, exercicio, null));
		}
	}

	@Permissao("Professor")
	public static Result formulario() {
		List<Curso> cursos = Curso.find.findList();
		return ok(formulario.render(formExer, cursos));
	}

	@Permissao("Professor")
	public static Result cadastrar() {
		Form<Exercicio> form = formExer.bindFromRequest();
		Long idCurso = Long.valueOf(form.data().get("idCurso"));

		if (form.hasErrors()) {
			List<Curso> cursos = Curso.find.findList();

			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form, cursos));
		}

		Exercicio exercicio = form.get();
		exercicio.autor = InformacoesUsuarioHelper.getUsuarioLogado();
		exercicio.curso = Curso.find.byId(idCurso);
		exercicio.save();

		flash().put(
				"success",
				"Exercício <strong>\"" + exercicio.titulo
						+ "\"</strong> cadastrado com sucesso!");
		return redirect(Exercicios.index());
	}

	@Permissao("Professor")
	public static Result formularioEdicao(Long id) {
		List<Curso> cursos = Curso.find.findList();
		Exercicio exercicio = Exercicio.find.byId(id);

		return ok(formularioEdicao.render(formExer.fill(exercicio), cursos,
				exercicio));
	}

	@Permissao("Professor")
	public static Result editar(Long id) {
		Form<Exercicio> form = formExer.bindFromRequest();
		Exercicio exercicio = Exercicio.find.byId(id);

		if (form.hasErrors()) {
			List<Curso> cursos = Curso.find.findList();

			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, cursos, exercicio));
		}
		Exercicio exer = form.get();

		exercicio.setCurso(Curso.find.byId(Long.valueOf(form.data().get(
				"idCurso"))));
		exercicio.setTitulo(exer.titulo);
		exercicio.setDescricao(exer.descricao);
		exercicio.setCasoDeTeste(exer.casoDeTeste);
		exercicio.setSolucaoProposta(exer.solucaoProposta);
		exercicio.setPontuacao(exer.pontuacao);
		exercicio.setNivelExercicio(exer.nivelExercicio);
		exercicio.setEstruturaMetodoProposto(exer.estruturaMetodoProposto);
		exercicio.update();

		flash().put(
				"success",
				"Exercício <strong>\"" + exercicio.titulo
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Exercicios.index());
	}

	@Permissao("Professor")
	public static Result deletar(Long id) {
		Exercicio exercicio = Exercicio.find.byId(id);

		if (exercicio == null) {
			flash().put("error",
					"O Exercício informado não foi encontrado no Sistema.");
		} else {
			exercicio.delete();

			flash().put(
					"success",
					"Exercicio <strong>\"" + exercicio.titulo
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Exercicios.index());
	}

	public static Result avaliarResposta(Long id) throws IOException {
		Form<ExercicioForm> form = formExercicio.bindFromRequest();
		ExercicioForm exercicioForm = form.get();
		Exercicio exercicio = Exercicio.find.byId(id);

		if (InformacoesUsuarioHelper.isExercicioResolvido(id)) {
			flash().put("error",
					"Você Já submeteu a resposta para este exercício!");
			return badRequest(visualizar.render(form, formDuvida, exercicio,
					null));
		}

		// Criando arquivo temporário no servidor
		File arquivo = File.createTempFile(
				"potigolcode" + session().hashCode(), ".poti");
		FileWriter writer = new FileWriter(arquivo);

		// Inserindo a resposta submetida pelo aluno
		writer.write(exercicioForm.resposta);
		writer.write("\n\n");

		// Inserindo o caso de teste cadastrado pelo professor
		writer.write(exercicio.casoDeTeste);
		writer.close();

		// Executando o teste automatizado
		Logger.info("Iniciando o teste unitário! \n");
		PotigolTestsHelper tester = new PotigolTestsHelper(arquivo);

		if (!tester.falhas.isEmpty()) {
			// TODO: Ajustar mensagem de erro da resposta.

			flash().put("error",
					"Infelizmente você não acertou a resolução para o Exercício. Tente novamente!");
			return badRequest(visualizar.render(form, formDuvida, exercicio,
					tester.resultadoTeste));
		}

		System.out.println("\n");
		Logger.info("Teste unitário concluído!");

		ExercicioResolvido exercicioResolvido = new ExercicioResolvido();
		exercicioResolvido.exercicio = exercicio;
		exercicioResolvido.usuario = InformacoesUsuarioHelper
				.getUsuarioLogado();
		exercicioResolvido.respostaSubmetida = exercicioForm.resposta;

		if (session().get("pontuacaoExercicio" + id) == null
				|| session().get("pontuacaoExercicio" + id).trim().equals("")) {
			exercicioResolvido.pontuacaoObtida = exercicio.pontuacao;
		} else {
			exercicioResolvido.pontuacaoObtida = Integer.valueOf(session().get(
					"pontuacaoExercicio" + id));
		}

		exercicioResolvido.save();

		session().remove("pontuacaoExercicio" + id);
		session().remove("mostrouSolucaoExercicio" + id);
		flash().put("success",
				"Parabéns! Você conseguiu resolver o exercício corretamente.");
		return ok(visualizar.render(form, formDuvida, exercicio,
				tester.resultadoTeste));
	}

	public static Result mostrarSolucao(Long id) throws InterruptedException {
		Exercicio exercicio = Exercicio.find.byId(id);

		if (exercicio == null) {
			flash().put("error",
					"O Exercício informado não foi encontrado no Sistema.");
			return redirect(Cursos.index());
		} else {
			session().put("mostrouSolucaoExercicio" + id, "true");
			session().put("pontuacaoExercicio" + id,
					exercicio.pontuacao - (exercicio.pontuacao / 4) + "");

			response().setContentType("text/plain");
			return ok(exercicio.solucaoProposta);
		}
	}

	public static Result descartarResposta(Long id) {
		ExercicioResolvido exercicioResolvido = InformacoesUsuarioHelper
				.getExercicioResolvido(id);

		if (exercicioResolvido == null) {
			flash().put("error",
					"O Exercício informado não foi encontrado no Sistema.");
			return redirect(Cursos.index());
		} else {
			exercicioResolvido.delete();

			flash().put(
					"success",
					"Resposta para o Exercício <strong>\""
							+ exercicioResolvido.exercicio.getTitulo()
							+ "\"</strong> descartada com sucesso!");
			return redirect(Exercicios
					.visualizar(exercicioResolvido.exercicio.id));
		}
	}

	public static Result enviarDuvida(Long id) {
		Form<DuvidaExercicioForm> form = formDuvida.bindFromRequest();
		DuvidaExercicioForm duvidaForm = form.get();
		Exercicio exercicio = Exercicio.find.byId(id);

		enviarDuvidaExercicio(
				InformacoesUsuarioHelper.getUsuarioLogado(), exercicio,
				duvidaForm.titulo, duvidaForm.mensagem);

		flash().put("success", "Dúvida enviada com sucesso!");
		return redirect(Exercicios.visualizar(id));
	}
}
