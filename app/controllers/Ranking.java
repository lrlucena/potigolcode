package controllers;

import static helpers.SegurancaHelpers.InformacoesUsuarioHelper.getPontuacaoUsuario;
import static helpers.SegurancaHelpers.InformacoesUsuarioHelper.getUsuarioLogado;
import helpers.PontuacaoComparator;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import models.ExercicioResolvido;
import models.Usuario;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Ranking.index;
import views.html.Ranking.meuProgresso;

public class Ranking extends Controller {
	public static Result index() {
		List<Usuario> alunos = Usuario.alunos();

		HashMap<Usuario, Integer> pontuacaoAlunos = new HashMap<>();
		PontuacaoComparator bvc = new PontuacaoComparator(pontuacaoAlunos);
		TreeMap<Usuario, Integer> rankingAlunos = new TreeMap<>(bvc);

		for (Usuario usuario : alunos) {
			pontuacaoAlunos.put(usuario, getPontuacaoUsuario(usuario.id));
		}

		rankingAlunos.putAll(pontuacaoAlunos);

		return ok(index.render(rankingAlunos));
	}

	public static Result meuProgresso() {
		List<ExercicioResolvido> progresso = getUsuarioLogado().progresso;
		return ok(meuProgresso.render(progresso));
	}
}