package controllers;

import static controllers.routes.Glossarios;
import helpers.SegurancaHelpers.InformacoesUsuarioHelper;
import helpers.SegurancaHelpers.Permissao;

import java.util.Date;

import models.CapituloGlossario;
import models.Glossario;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.CapitulosGlossario.formulario;
import views.html.CapitulosGlossario.formularioEdicao;

public class CapitulosGlossario extends Controller {
	private static Form<CapituloGlossario> formCG = Form
			.form(CapituloGlossario.class);

	@Permissao("Professor")
	public static Result formulario(Long idGlossario) {
		Glossario glossario = Glossario.find.byId(idGlossario);
		return ok(formulario.render(formCG, glossario));
	}

	@Permissao("Professor")
	public static Result cadastrar(Long idGlossario) {
		Form<CapituloGlossario> form = formCG.bindFromRequest();
		Glossario glossario = Glossario.find.byId(idGlossario);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form, glossario));
		}

		CapituloGlossario capituloGlossario = form.get();
		capituloGlossario.autor = InformacoesUsuarioHelper.getUsuarioLogado();
		capituloGlossario.dataCadastro = new Date();
		capituloGlossario.glossario = glossario;

		capituloGlossario.save();

		flash().put(
				"success",
				"Capótulo de Glossário <strong>\"" + glossario.titulo
						+ "\"</strong> cadastrado com sucesso!");
		return redirect(Glossarios.visualizar(glossario.id));
	}

	@Permissao("Professor")
	public static Result formularioEdicao(Long idCapituloGlossario) {
		CapituloGlossario capituloGlossario = CapituloGlossario.find
				.byId(idCapituloGlossario);
		return ok(formularioEdicao.render(formCG.fill(capituloGlossario),
				capituloGlossario));
	}

	@Permissao("Professor")
	public static Result editar(Long idCapituloGlossario) {
		Form<CapituloGlossario> form = formCG.bindFromRequest();
		CapituloGlossario capitulo = CapituloGlossario.find
				.byId(idCapituloGlossario);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, capitulo));
		}

		capitulo.setTitulo(form.get().titulo);
		capitulo.setConteudo(form.get().conteudo);
		capitulo.update();

		flash().put(
				"success",
				"Capítulo <strong>\"" + capitulo.titulo
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Glossarios.visualizar(capitulo.glossario.id));
	}

	@Permissao("Professor")
	public static Result deletar(Long idCapituloGlossario) {
		CapituloGlossario capitulo = CapituloGlossario.find
				.byId(idCapituloGlossario);

		if (capitulo == null) {
			flash().put("error",
					"O Capítulo informado não foi encontrado no Sistema.");
		} else {
			capitulo.delete();

			flash().put(
					"success",
					"Capítulo <strong>\"" + capitulo.titulo
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Glossarios.visualizar(capitulo.glossario.id));
	}
}
