package controllers;

import static controllers.routes.Administracao;
import static controllers.routes.Application;
import static controllers.routes.Sessions;
import helpers.SegurancaHelpers.Autenticar;
import helpers.SegurancaHelpers.InformacoesUsuarioHelper;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import models.Usuario;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Sessions.esqueciSenha;
import views.html.Sessions.formularioAlteracaoSenha;
import views.html.Sessions.login;
import akka.util.Crypt;
import emails.RegistroMailer;
import forms.AlterarSenhaForm;
import forms.LoginForm;


/**
 * Classe responsável pelo controle da autenticação dos usuários, recuperação e
 * alteração da senha.
 * 
 * @author allysonbarros
 */
public class Sessions extends Controller {
	private static Form<LoginForm> formLogin = Form.form(LoginForm.class);
	private static Form<AlterarSenhaForm> formSenha = Form.form(AlterarSenhaForm.class);
	private static Form<Usuario> formUsuario = Form.form(Usuario.class);
	
	/**
	 * Método responsável pela renderização do formulário de Login
	 * 
	 * @return
	 */
	public static Result login() {
		return ok(login.render(formLogin));
	}

	/**
	 * Método responsável pelo controle do login dos usuários
	 * 
	 * @return
	 */
	public static Result efetuarLogin() throws NoSuchAlgorithmException {
		Form<LoginForm> form = formLogin.bindFromRequest();
		String login = form.field("login").value();

		if (form.hasErrors()) {
			flash("error", "Login ou Senha Inválida(s). Tente novamente!");
			return redirect(Sessions.login());
		} else {
			Usuario usuario = Usuario.find.where().ilike("login", login)
					.findUnique();

			if (!usuario.isAtivo) {
				flash("error",
						"Você precisa ativar sua conta para poder acessar o sistema. "
								+ "<a href=\""
								+ Sessions.ativarConta(usuario.email)
										.absoluteURL(request())
								+ "\">Clique AQUI para ativar sua conta!</a>");
				return redirect(Application.index());
			} else {
				session().put("usuarioLogadoID", usuario.id.toString());

				if (usuario.isProfessor)
					return redirect(Administracao.index());
				else
					return redirect(Application.index());
			}
		}
	}

	/**
	 * Método responsável pela remoção do usuário logado da sessão.
	 * 
	 * @return
	 */
	@Autenticar
	public static Result efetuarLogoff() {
		session().remove("usuarioLogadoID");
		flash().put("success", "Você foi desconectado do sistema!");

		return redirect(Application.index());
	}

	@Autenticar
	public static Result formularioAlteracaoSenha() {
		return ok(formularioAlteracaoSenha.render(formSenha));
	}

	@Autenticar
	public static Result alterarSenha() throws NoSuchAlgorithmException {
		AlterarSenhaForm form = formSenha.bindFromRequest().get();
		Usuario usuarioLogado = InformacoesUsuarioHelper.getUsuarioLogado();

		if (Usuario.criptografarSenha(form.senhaAtual).equals(
				usuarioLogado.senha)
				&& form.novaSenha.equals(form.novaSenhaConfirmacao)) {
			usuarioLogado.setSenha(Usuario.criptografarSenha(form.novaSenha));
			usuarioLogado.update();

			session().remove("usuarioLogadoID");

			flash().put(
					"success",
					"Sua senha foi alterada com sucesso. <br/>Você precisa acessar sua conta novamente.");
			return redirect(Sessions.login());
		} else if (!form.novaSenha.equals(form.novaSenhaConfirmacao)) {
			flash().put("error",
					"As senhas informadas não conferem. Tente novamente!");
			return badRequest(formularioAlteracaoSenha.render(formSenha));
		} else {
			flash().put("error",
					"As senha atual informada não confere. Tente novamente!");
			return badRequest(formularioAlteracaoSenha.render(formSenha));
		}
	}

	/**
	 * Método responsável pela renderização do formulário de Esqueci a Senha.
	 * 
	 * @return
	 */
	public static Result esqueciSenha() {
		return ok(esqueciSenha.render(formUsuario));
	}

	/**
	 * Método responsável pelo envio da chave de redefinição de senha para o
	 * email do usuário. A senha gerada é gravada na conta do usuário
	 * 
	 * @return
	 */
	public static Result solicitarRedefinicaoSenha() {
		Form<Usuario> form = formUsuario.bindFromRequest();
		String email = form.field("email").value();

		Usuario usuario = Usuario.find.where().eq("email", email).findUnique();

		if (usuario == null) {
			flash().put("error",
					"Email inválido ou não cadastrado. Tente novamente!");
			return ok(esqueciSenha.render(formUsuario));
		} else {
			usuario.setChaveRedefinicaoSenha(Crypt.generateSecureCookie());
			usuario.update();

			flash().put(
					"success",
					"Enviamos um email para o endereço: <strong>"
							+ usuario.email
							+ "</strong>."
							+ "<br/>Nele você receberá as intruções para a redefinição de sua senha");

			RegistroMailer.enviarSolicitacaoRedefinicaoSenha(usuario);
			return ok(esqueciSenha.render(formUsuario));
		}
	}

	/**
	 * Método responsável por resetar a senha do usuário. A senha é resetada
	 * somente se a chave de redefinição da solicitação for igual a chave do
	 * usuário no banco de dados.
	 * 
	 * @param chave
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static Result redefinirSenha(String chave)
			throws NoSuchAlgorithmException {
		Usuario usuario = Usuario.find.where()
				.eq("chaveRedefinicaoSenha", chave).findUnique();

		if (usuario == null) {
			flash().put("error",
					"Chave para redefinição de senha não encontrada ou inválida!");
			return ok(esqueciSenha.render(formUsuario));
		} else {
			String novaSenha = UUID.randomUUID().toString().substring(0, 8);

			usuario.setSenha(Usuario.criptografarSenha(novaSenha));
			usuario.setChaveRedefinicaoSenha(null);
			usuario.update();

			flash().put(
					"success",
					String.format(
							"Enviamos sua nova senha para uma email para o endereço: <strong>%s</strong>. ",
							usuario.email));
			RegistroMailer.enviarSenhaRedefinida(usuario, novaSenha);

			return ok(login.render(formLogin));
		}
	}

	public static Result ativarConta(String email) {
		Usuario usuario = Usuario.find.where().eq("email", email).findUnique();
		usuario.isAtivo = true;
		usuario.update();

		flash().put(
				"success",
				String.format("Sua conta foi ativada com sucesso! \nAgora você poderá desfrutar de todas as funcionalidades do sistema."));
		return ok(login.render(formLogin));
	}
}
