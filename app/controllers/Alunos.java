package controllers;

import static controllers.routes.Alunos;
import static controllers.routes.Application;
import helpers.SegurancaHelpers.Permissao;

import java.util.List;

import models.Usuario;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Alunos.formRegistro;
import views.html.Alunos.formulario;
import views.html.Alunos.formularioEdicao;
import views.html.Alunos.index;
import emails.RegistroMailer;
import forms.AlterarUsuarioForm;

public class Alunos extends Controller {
	private static Form<Usuario> formUsuario = Form.form(Usuario.class);
	private static Form<AlterarUsuarioForm> formAlterar = Form
			.form(AlterarUsuarioForm.class);

	public static Result index() {
		List<Usuario> alunos = Usuario.alunos();
		return ok(index.render(alunos));
	}

	public static Result formulario() {
		return ok(formulario.render(formUsuario));
	}

	public static Result formRegistro() {
		return ok(formRegistro.render(formUsuario));
	}

	public static Result cadastrarAluno() {
		Form<Usuario> form = formUsuario.bindFromRequest();

		if (form.hasErrors()
				|| !form.get().senha
						.equals(form.data().get("confirmacaoSenha"))) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formRegistro.render(form));
		}

		Usuario aluno = form.get();
		aluno.isAtivo = false;
		aluno.save();

		// Envia o email de confirmação de cadastro no sistema!
		RegistroMailer.enviarMensagemRegistro(aluno);

		flash().put("success", "Conta registrada com sucesso!");
		return redirect(Application.index());
	}

	public static Result cadastrar() {
		Form<Usuario> form = formUsuario.bindFromRequest();

		if (form.hasErrors()
				|| !form.get().senha
						.equals(form.data().get("confirmacaoSenha"))) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form));
		}

		Usuario aluno = form.get();
		aluno.isAtivo = false;
		aluno.save();

		// Envia o email de confirmação de cadastro no sistema!
		RegistroMailer.enviarMensagemRegistro(aluno);

		flash().put("success", "Conta registrada com sucesso!");
		return redirect(Alunos.index());
	}

	@Permissao("Professor")
	public static Result formularioEdicao(Long id) {
		Usuario aluno = Usuario.find.byId(id);
		AlterarUsuarioForm formulario = new AlterarUsuarioForm();
		formulario.nome = aluno.nome;
		formulario.email = aluno.email;

		return ok(formularioEdicao.render(formAlterar.fill(formulario), aluno));
	}

	@Permissao("Professor")
	public static Result editar(Long id) {
		Form<AlterarUsuarioForm> form = formAlterar.bindFromRequest();
		Usuario aluno = Usuario.find.byId(id);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, aluno));
		}

		aluno.setNome(form.get().nome);
		aluno.setEmail(form.get().email);
		aluno.update();

		flash().put(
				"success",
				"Aluno <strong>\"" + aluno.nome
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Alunos.index());
	}

	@Permissao("Professor")
	public static Result deletar(Long id) {
		Usuario aluno = Usuario.find.byId(id);

		if (aluno == null) {
			flash().put("error",
					"O Aluno informado não foi encontrado no Sistema.");
		} else {
			aluno.delete();

			RegistroMailer.enviarMensagemExclusao(aluno);

			flash().put(
					"success",
					"Aluno <strong>\"" + aluno.nome
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Alunos.index());
	}
}
