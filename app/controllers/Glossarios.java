package controllers;

import java.util.Date;
import java.util.List;

import helpers.SegurancaHelpers.InformacoesUsuarioHelper;
import helpers.SegurancaHelpers.Permissao;
import models.Glossario;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import views.html.Glossarios.*;

public class Glossarios extends Controller {
	private static Form<Glossario> formGlossario = Form.form(Glossario.class);
	public static Result index() {
		List<Glossario> glossarios = Glossario.find.all();
		
		return ok(index.render(glossarios));
	}

	public static Result visualizar(Long id) {
		Glossario glossario = Glossario.find.byId(id);
		
		return ok(visualizar.render(glossario));
	}
	
	@Permissao("Professor")
	public static Result formulario() {
		return ok(formulario.render(formGlossario));
	}
	
	@Permissao("Professor")
	public static Result cadastrar() {
		Form<Glossario> form = formGlossario.bindFromRequest();
		
		if(form.hasErrors()) {
			flash().put("error", "Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form));
		}
		
		Glossario glossario = form.get();
		glossario.autor = InformacoesUsuarioHelper.getUsuarioLogado();
		glossario.dataCadastro = new Date();
		
		glossario.save();
		
		flash().put("success", "Glossário <strong>\""+ glossario.titulo +"\"</strong> cadastrado com sucesso!");
		return redirect(routes.Glossarios.index());
	}
	
	@Permissao("Professor")
	public static Result formularioEdicao(Long id) {
		Glossario glossario = Glossario.find.byId(id);
		return ok(formularioEdicao.render(formGlossario.fill(glossario), glossario));
	}
	
	@Permissao("Professor")
	public static Result editar(Long id) {
		Form<Glossario> form = formGlossario.bindFromRequest();
		Glossario glossario = Glossario.find.byId(id);
		
		if(form.hasErrors()) {
			flash().put("error", "Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, glossario));
		}
		
		glossario.setTitulo(form.get().titulo);
		glossario.setDescricao(form.get().descricao);
		glossario.update();
		
		flash().put("success", "Glossário <strong>\""+ glossario.titulo +"\"</strong> atualizado com sucesso!");
		return redirect(routes.Glossarios.index());
	}
	
	@Permissao("Professor")
	public static Result deletar(Long id) {
		Glossario glossario = Glossario.find.byId(id);
		
		if (glossario == null) {
			flash().put("error", "O Glossário informado não foi encontrado no Sistema.");
		} else {
			glossario.delete();
			
			flash().put("success", "Glossário <strong>\""+ glossario.titulo +"\"</strong> excluído com sucesso!");
		}
		
		return redirect(routes.Glossarios.index());
	}
}
