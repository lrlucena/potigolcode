package controllers;

import static controllers.routes.Professores;
import helpers.SegurancaHelpers.Permissao;

import java.util.List;

import models.Usuario;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.Professores.formulario;
import views.html.Professores.formularioEdicao;
import views.html.Professores.index;
import emails.RegistroMailer;
import forms.AlterarUsuarioForm;

public class Professores extends Controller {
	private static Form<Usuario> formBranco = Form.form(Usuario.class);
	private static Form<AlterarUsuarioForm> formAlterar = Form
			.form(AlterarUsuarioForm.class);

	public static Result index() {
		List<Usuario> professores = Usuario.professores();
		return ok(index.render(professores));
	}

	public static Result formulario() {
		return ok(formulario.render(formBranco));
	}

	public static Result cadastrar() {
		Form<Usuario> form = formBranco.bindFromRequest();

		if (form.hasErrors()
				|| !form.get().senha
						.equals(form.data().get("confirmacaoSenha"))) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formulario.render(form));
		}

		Usuario professor = form.get();
		professor.isAtivo = false;
		professor.isProfessor = true;
		professor.save();

		// Envia o email de confirmação de cadastro no sistema!
		RegistroMailer.enviarMensagemRegistro(professor);

		flash().put(
				"success",
				"Professor <strong>\"" + professor.nome
						+ "\"</strong> cadastrado com sucesso!");
		return redirect(Professores.index());
	}

	@Permissao("Professor")
	public static Result formularioEdicao(Long id) {
		Usuario professor = Usuario.find.byId(id);
		AlterarUsuarioForm formulario = new AlterarUsuarioForm();
		formulario.nome = professor.nome;
		formulario.email = professor.email;

		return ok(formularioEdicao.render(formAlterar.fill(formulario),
				professor));
	}

	@Permissao("Professor")
	public static Result editar(Long id) {
		Form<AlterarUsuarioForm> form = formAlterar.bindFromRequest();
		Usuario professor = Usuario.find.byId(id);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, professor));
		}

		professor.setNome(form.get().nome);
		professor.setEmail(form.get().email);
		professor.update();

		flash().put(
				"success",
				"Aluno <strong>\"" + professor.nome
						+ "\"</strong> atualizado com sucesso!");
		return redirect(Professores.index());
	}

	@Permissao("Professor")
	public static Result deletar(Long id) {
		Usuario professor = Usuario.find.byId(id);

		if (professor == null) {
			flash().put("error",
					"O Aluno informado não foi encontrado no Sistema.");
		} else {
			professor.delete();

			RegistroMailer.enviarMensagemExclusao(professor);

			flash().put(
					"success",
					"Aluno <strong>\"" + professor.nome
							+ "\"</strong> excluído com sucesso!");
		}

		return redirect(Professores.index());
	}
}
