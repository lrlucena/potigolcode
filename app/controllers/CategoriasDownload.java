package controllers;

import static controllers.routes.CategoriasDownload;
import helpers.SegurancaHelpers.Permissao;

import java.util.List;

import models.CategoriaDownload;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.CategoriasDownload.formulario;
import views.html.CategoriasDownload.formularioEdicao;
import views.html.CategoriasDownload.index;

public class CategoriasDownload extends Controller {
	private static Form<CategoriaDownload> formCat = Form
			.form(CategoriaDownload.class);

	@Permissao("Professor")
	public static Result index() {
		List<CategoriaDownload> categorias = CategoriaDownload.find.findList();
		return ok(index.render(categorias));
	}

	@Permissao("Professor")
	public static Result formulario() {
		return ok(formulario.render(formCat));
	}

	@Permissao("Professor")
	public static Result cadastrar() {
		Form<CategoriaDownload> form = formCat.bindFromRequest();

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return ok(formulario.render(form));
		}

		CategoriaDownload categoria = form.get();

		categoria.save();

		flash().put(
				"success",
				"Categoria de Download <strong>\"" + categoria.nome
						+ "\"</strong> cadastrada com sucesso!");
		return redirect(CategoriasDownload.index());
	}

	public static Result formularioEdicao(Long id) {
		CategoriaDownload categoria = CategoriaDownload.find.byId(id);
		return ok(formularioEdicao.render(formCat.fill(categoria), categoria));
	}

	public static Result editar(Long id) {
		Form<CategoriaDownload> form = formCat.bindFromRequest();
		CategoriaDownload categoria = CategoriaDownload.find.byId(id);

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente. Tente novamente!");
			return badRequest(formularioEdicao.render(form, categoria));
		}

		categoria.setNome(form.get().nome);
		categoria.setDescricao(form.get().descricao);

		categoria.update();

		flash().put(
				"success",
				"Categoria de Download <strong>\"" + categoria.nome
						+ "\"</strong> atualizado com sucesso!");
		return redirect(CategoriasDownload.index());
	}

	@Permissao("Professor")
	public static Result deletar(Long id) {
		CategoriaDownload categoria = CategoriaDownload.find.byId(id);

		if (categoria == null) {
			flash().put("error",
					"A Categoria de Download informada não foi encontrada no Sistema.");
		} else {
			categoria.delete();

			flash().put(
					"success",
					"Categoria de Download <strong>\"" + categoria.nome
							+ "\"</strong> excluída com sucesso!");
		}

		return redirect(CategoriasDownload.index());
	}
}
