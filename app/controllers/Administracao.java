package controllers;

import helpers.SegurancaHelpers.Permissao;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

public class Administracao extends Controller {
	@Permissao("Professor")
	public static Result index() {
		return ok(index.render());
	}
}
