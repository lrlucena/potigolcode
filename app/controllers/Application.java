package controllers;

import static emails.ApplicationMailer.enviarMensagemContato;

import java.util.List;

import models.CategoriaDownload;
import models.Download;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.contato;
import views.html.downloads;
import views.html.index;
import forms.ContatoForm;

/**
 * Controlador responsável pela área pública do Sistema.<br/>
 * Ações:
 * 
 * <ul>
 * <li>Index</li>
 * <li>Sobre</li>
 * <li>Downloads</li>
 * <li>Contato</li>
 * </ul>
 * 
 * @author allysonbarros
 */
public class Application extends Controller {
	private static Form<ContatoForm> formCont = Form.form(ContatoForm.class);

	public static Result index() {
		return ok(index.render());
	}

	public static Result downloads() {
		scala.Option<CategoriaDownload> cd = scala.Option.apply(null);
		return ok(downloads.render(Download.todos(), CategoriaDownload.todos(),
				cd));
	}

	public static Result downloadsPorCategoria(Long idCategoria) {
		List<Download> download = Download.categoria(idCategoria);
		List<CategoriaDownload> categorias = CategoriaDownload.find.findList();
		CategoriaDownload categoriaSelecionada = CategoriaDownload.find
				.byId(idCategoria);

		return ok(downloads.render(download, categorias,
				scala.Some.apply(categoriaSelecionada)));
	}

	public static Result contato() {
		return ok(contato.render(formCont));
	}

	public static Result enviarContato() {
		Form<ContatoForm> form = formCont.bindFromRequest();

		if (form.hasErrors()) {
			flash().put("error",
					"Você deve preencher todos os campos corretamente!");
		} else {
			String nome = form.field("nome").value();
			String email = form.field("email").value();
			String twitter = form.field("twitter").value();
			String mensagem = form.field("mensagem").value();

			enviarMensagemContato(nome, email, twitter, mensagem);
			enviarMensagemContato(nome, email);

			flash().put("success", "Mensagem de contato enviada com sucesso!");
		}

		return Application.contato();
	}
}