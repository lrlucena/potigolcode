package emails;

import java.util.List;

import models.Exercicio;
import models.Usuario;
import views.html.Emails.ApplicationMailer.mensagemContato;
import views.html.Emails.ApplicationMailer.mensagemContatoRemetente;
import views.html.Emails.ApplicationMailer.mensagemEnvioDuvida;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;

/**
 * Classe responsável pelo envio de emails de notificação de contato.
 * 
 * @author allysonbarros
 * 
 */
public class ApplicationMailer {
	private static MailerPlugin mailer = play.Play.application().plugin(
			MailerPlugin.class);

	public static void enviarMensagemContato(String nome, String email,
			String twitter, String mensagem) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = mensagemContato.render(nome, email, twitter,
				mensagem).toString();

		mail.setCharset("UTF-8");
		mail.setSubject(String.format("PotigolCode - %s entrou em contato!",
				nome));
		mail.setRecipient("potigolcode@gmail.com");
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}

	public static void enviarMensagemContato(String nome, String email) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = mensagemContatoRemetente.render(nome).toString();

		mail.setCharset("UTF-8");
		mail.setSubject(String.format(
				"PotigolCode - %s, obrigado por entrar em contato conosco!",
				nome));
		mail.setRecipient(email);
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}

	public static void enviarDuvidaExercicio(Usuario usuario,
			Exercicio exercicio, String titulo, String mensagem) {
		List<Usuario> professores = Usuario.find.where()
				.eq("isProfessor", true).findList();
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = mensagemEnvioDuvida.render(usuario, exercicio,
				titulo, mensagem).toString();

		mail.setCharset("UTF-8");
		mail.setSubject(String.format("PotigolCode - %s enviou uma dúvida!",
				usuario.nome));

		for (Usuario professor : professores) {
			mail.setRecipient(professor.email);
		}

		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}
}
