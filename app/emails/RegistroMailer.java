package emails;

import models.Usuario;
import views.html.Emails.RegistroMailer.mensagemExclusao;
import views.html.Emails.RegistroMailer.mensagemRegistro;
import views.html.Emails.RegistroMailer.redefinirSenha;
import views.html.Emails.RegistroMailer.solicitacaoRedefinicaoSenha;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;

/**
 * Classe responsável pelo envio de emails de notificação de registro,
 * recuperação de senha e alteração de senha.
 * 
 * @author allysonbarros
 * 
 */
public class RegistroMailer {
	private static MailerPlugin mailer = play.Play.application().plugin(
			MailerPlugin.class);

	public static void enviarMensagemRegistro(Usuario usuario) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = mensagemRegistro.render(usuario).toString();

		mail.setCharset("UTF-8");
		mail.setSubject("PotigolCode - Bem vindo!");
		mail.setRecipient(usuario.email);
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}

	public static void enviarSolicitacaoRedefinicaoSenha(Usuario usuario) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = solicitacaoRedefinicaoSenha.render(usuario)
				.toString();

		mail.setCharset("UTF-8");
		mail.setSubject("PotigolCode - Solicitação de Redefinição de Senha!");
		mail.setRecipient(usuario.email);
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}

	public static void enviarSenhaRedefinida(Usuario usuario, String novaSenha) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = redefinirSenha.render(usuario, novaSenha)
				.toString();

		mail.setCharset("UTF-8");
		mail.setSubject("PotigolCode - Solicitação de Redefinição de Senha!");
		mail.setRecipient(usuario.email);
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}

	public static void enviarMensagemExclusao(Usuario usuario) {
		MailerAPI mail = mailer.email();

		// Pega o conteúdo da view e joga numa string, utilizando assim os
		// conceitos do MVC.
		String mensagemHTML = mensagemExclusao.render(usuario).toString();

		mail.setCharset("UTF-8");
		mail.setSubject("PotigolCode - Conta Desativada!");
		mail.setRecipient(usuario.email);
		mail.setFrom("[PotigolCode] <nao-reponda@potigolcode.com.br>");

		mail.sendHtml(mensagemHTML);
	}
}
