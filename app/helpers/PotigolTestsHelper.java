package helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PotigolTestsHelper {
	private static final String CAMINHO_BIBLIOTECA_POTIGOL = "potigol/lib/potigol.jar";

	private String bin;
	private File file;
	private int quantidadeTestes;

	private List<String> nomeMetodos;
	public List<String> falhas;
	public List<ResultadoTeste> resultadoTeste;

	public PotigolTestsHelper(File file) {
		this.bin = String.format("java -jar %s", CAMINHO_BIBLIOTECA_POTIGOL);
		this.file = file;
		this.quantidadeTestes = 0;

		this.nomeMetodos = new ArrayList<>();
		this.falhas = new ArrayList<>();
		this.resultadoTeste = new ArrayList<>();

		try {
			run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void run() throws IOException {
		FileReader freader = null;

		try {
			freader = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		BufferedReader reader = new BufferedReader(freader);

		String line = null;
		String expected = "";
		String actual = "";

		System.out.println(".: Potigol Unit Test :.");
		System.out.println("");
		System.out.print("[" + file.getAbsolutePath() + "]\n");

		while ((line = reader.readLine()) != null)
			if (line.indexOf("=>") != -1) {
				int indexOf = line.indexOf("=>");
				String t = line
						.substring(line.indexOf("=>") + 2, line.length())
						.trim();

				if (!t.equals("")) {
					quantidadeTestes++;
					expected += t.concat(", ");
					nomeMetodos.add(line.substring(1, indexOf).trim());
				}
			}

		actual += test(file);

		// Capturando os resultados dos testes
		String[] esperados = expected.split(", ");
		String[] recebidos = actual.split(", ");
		if (esperados.length > 0) {
			for (int i = 0; i < esperados.length; i++) {
				String esperado = esperados[i];
				String recebido;

				try {
					recebido = recebidos[i];
				} catch (ArrayIndexOutOfBoundsException e) {
					recebido = "";
				}

				if (!esperado.equals(recebido)) {
					falhas.add("\tEsperado: " + esperado + "\n\tRecebido: "
							+ recebido);
					resultadoTeste.add(new ResultadoTeste(nomeMetodos.get(i),
							esperado, recebido, false));
				} else {
					resultadoTeste.add(new ResultadoTeste(nomeMetodos.get(i),
							esperado, recebido, true));
				}
			}
		}

		if (falhas.isEmpty()) {
			pass();
		}

		reader.close();
		freader.close();

		for (String falha : falhas) {
			System.out.print("\n" + falha + "\n");
		}

		System.out.println("\nTestes: " + quantidadeTestes + "; Falhas: "
				+ falhas.size() + ".");
	}

	private String test(File file) {
		String result = "";
		try {
			Process process = Runtime.getRuntime().exec(
					bin + " " + file.getPath());
			BufferedReader in = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String inputLine = null;
			while ((inputLine = in.readLine()) != null) {
				result += inputLine.concat(", ");
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	private void pass() {
		System.out.print("\nSucesso! Todos os testes passaram.\n");
	}

	private void fail(File file, String expected, String actual) {
		String[] esperados = expected.split(", ");
		String[] recebidos = actual.split(", ");

		for (int i = 0; i < esperados.length; i++) {
			String esperado = esperados[i];
			String recebido;

			try {
				recebido = recebidos[i];
			} catch (ArrayIndexOutOfBoundsException e) {
				recebido = "";
			}

			if (!esperado.equals(recebido)) {
				falhas.add("\tEsperado: " + esperado + "\n\tRecebido: "
						+ recebido);
			}
		}
	}

	public static void main(String[] args) {
		new PotigolTestsHelper(new File("potigol/sleepIn.poti"));
	}
}
