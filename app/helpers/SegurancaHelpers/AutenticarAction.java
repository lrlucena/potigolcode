package helpers.SegurancaHelpers;

import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.SimpleResult;

/**
 * Helper que restringe o acesso somente para usuários autenticados no sistema. <br/>
 * <br/>
 * <i>Atenção: Utilizar a Anotação <strong>@Autenticar</strong> em cada método
 * onde o acesso seja restrito.</i>
 * 
 * @author allysonbarros
 */
public class AutenticarAction extends Action.Simple {
	@Override
	public Promise<SimpleResult> call(Context ctx) throws Throwable {
		if (ctx.session().get("usuarioLogadoID") == null) {
			ctx.flash().put(
					"url",
					"GET".equals(ctx.request().method()) ? ctx.request().uri()
							: "/");
			return null;// redirect(controllers.routes.Sessions.login());
		}

		return delegate.call(ctx);
	}
}
