package helpers.SegurancaHelpers;

import models.ExercicioResolvido;
import models.Usuario;
import play.mvc.Controller;

public class InformacoesUsuarioHelper extends Controller {
	public static Usuario getUsuarioLogado() {
		Long idUsuario = Long.parseLong(session("usuarioLogadoID"));

		return Usuario.find.byId(idUsuario);
	}

	public static Boolean isLogado() {
		return session().get("usuarioLogadoID") != null;
	}

	public static Boolean isExercicioResolvido(Long idExercicio) {
		Usuario usuarioLogado = getUsuarioLogado();

		int findRowCount = ExercicioResolvido.find.where()
				.eq("usuario_id", usuarioLogado.id)
				.eq("exercicio_id", idExercicio).findRowCount();

		return findRowCount != 0;
	}

	public static ExercicioResolvido getExercicioResolvido(Long idExercicio) {
		Usuario usuarioLogado = getUsuarioLogado();

		ExercicioResolvido exercicioResolvido = ExercicioResolvido.find.where()
				.eq("usuario_id", usuarioLogado.id)
				.eq("exercicio_id", idExercicio).findUnique();

		return exercicioResolvido;
	}

	public static int getPontuacaoUsuario() {
		Usuario usuarioLogado = getUsuarioLogado();
		int pontuacao = 0;

		for (ExercicioResolvido exercicioResolvido : usuarioLogado.progresso) {
			pontuacao += exercicioResolvido.pontuacaoObtida;
		}

		return pontuacao;
	}

	public static int getPontuacaoUsuario(Long id) {
		Usuario usuario = Usuario.find.byId(id);
		int pontuacao = 0;

		for (ExercicioResolvido exercicioResolvido : usuario.progresso) {
			pontuacao += exercicioResolvido.pontuacaoObtida;
		}

		return pontuacao;
	}
}
