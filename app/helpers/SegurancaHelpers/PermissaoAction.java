package helpers.SegurancaHelpers;

//import controllers.ReverseApplication;
import models.Usuario;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.SimpleResult;

/**
 * Helper que restringe o acesso somente para usuários que forem administradores
 * do Sistema. <br/>
 * <br/>
 * <i>Atenção: Utilizar a Anotação <strong>@Permissao("Professor")</strong> em
 * cada método onde o acesso seja restrito.</i>
 * 
 * @author allysonbarros
 */
public class PermissaoAction extends Action<Permissao> {
	@Override
	public Promise<SimpleResult> call(Context ctx) throws Throwable {
		Usuario usuario;

		try {
			usuario = Usuario.find.byId(Long.valueOf(ctx.session().get(
					"usuarioLogadoID")));
		} catch (NumberFormatException e) {
			usuario = null;
		}

		if (usuario == null) {
			ctx.flash().put(
					"url",
					"GET".equals(ctx.request().method()) ? ctx.request().uri()
							: "/");
			// return redirect(null);//controllers.routes.Sessions.login());
		} else if (configuration.value().equalsIgnoreCase("Professor")
				&& !usuario.isProfessor) {
			ctx.flash().put("error",
					"Você não tem permissão para acessar este conteúdo!");
			// return redirect(null);//controllers.routes.Application.index());
		}

		return delegate.call(ctx);
	}
}
