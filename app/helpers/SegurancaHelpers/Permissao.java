package helpers.SegurancaHelpers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.mvc.With;

/**
 * Anotação responsável por abstrair a chamada da classe
 * <strong>PermissaoAction.java</strong>, que restringe o acesso somente para
 * usuários autenticados e que são administradores do sistema.
 * 
 * @author allysonbarros
 */
@With(PermissaoAction.class)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Permissao {
	String value() default "";
}