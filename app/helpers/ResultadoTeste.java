package helpers;

public class ResultadoTeste {
	public String nomeMetodo;
	public String valorEsperado;
	public String valorRecebido;
	public boolean testePassou;

	public ResultadoTeste(String nomeMetodo, String valorEsperado,
			String valorRecebido, boolean testePassou) {
		this.nomeMetodo = nomeMetodo;
		this.valorEsperado = valorEsperado;
		this.valorRecebido = valorRecebido;
		this.testePassou = testePassou;
	}
}