package helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import play.Logger;

/**
 * Classe que auxilia a execução de comandos de shell pela aplicação.
 * 
 * @author allysonbarros
 * 
 */
public class CommandHelper {
	/**
	 * Método que executa comandos de shell.
	 * 
	 * @param cmd
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void executeCommand(String cmd) throws IOException,
			InterruptedException {
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();

		BufferedReader buf = new BufferedReader(new InputStreamReader(
				pr.getInputStream()));
		String line = "";

		while ((line = buf.readLine()) != null) {
			Logger.info("Resultado: " + line);
		}
	}

	/**
	 * Método que executa comandos de shell e retorna uma string com o
	 * resultado.
	 * 
	 * @param cmd
	 * @return String
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String executeCommandWithResult(String cmd)
			throws IOException, InterruptedException {
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();

		BufferedReader buf = new BufferedReader(new InputStreamReader(
				pr.getInputStream()));
		String line = "";
		StringBuilder sb = new StringBuilder();

		while ((line = buf.readLine()) != null) {
			sb.append(line + "\n");
			Logger.info("Resultado: " + line);
		}

		return sb.toString();
	}
}
