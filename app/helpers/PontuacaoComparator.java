package helpers;

import java.util.Comparator;
import java.util.Map;

import models.Usuario;

public class PontuacaoComparator implements Comparator<Usuario> {
	Map<Usuario, Integer> base;

	public PontuacaoComparator(Map<Usuario, Integer> base) {
		this.base = base;
	}

	@Override
	public int compare(Usuario o1, Usuario o2) {
		if (base.get(o1) >= base.get(o2)) {
			return -1;
		} else {
			return 1;
		}
	}
}