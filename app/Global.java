import java.util.ArrayList;
import java.util.Date;

import models.CategoriaDownload;
import models.Glossario;
import models.Usuario;
import play.Application;
import play.GlobalSettings;
import play.Logger;

public class Global extends GlobalSettings {
	@Override
	public void onStart(Application app) {
		if (Usuario.find.all().size() == 0) {
			Usuario administrador = new Usuario();
			administrador.nome = "Allyson Barros";
			administrador.email = "allysonbarrosrn@gmail.com";
			administrador.login = "allysonbarros";
			administrador.senha = "18273645";
			administrador.isAtivo = true;
			administrador.isProfessor = true;
			
			administrador.save();
			
			Logger.info("Cadastrando os Administradores do Sistema.");
		}
		
		if (CategoriaDownload.find.all().size() == 0) {
			CategoriaDownload categoria = new CategoriaDownload();
			categoria.nome = "Ambientes de Desenvolvimento";
			categoria.save();
			
			CategoriaDownload categoria2 = new CategoriaDownload();
			categoria2.nome = "Editores de Texto";
			categoria2.save();
			
			CategoriaDownload categoria3 = new CategoriaDownload();
			categoria3.nome = "Apostilas e Livros";
			categoria3.save();
			
			Logger.info("Cadastrando as Categorias de Download do Sistema.");
		}
		
		if (Glossario.find.all().size() == 0) {
			Glossario glossario = new Glossario();
			
			glossario.titulo = "Glossário de Potigol";
			glossario.descricao = "Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.<br/>Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.";
			glossario.dataCadastro = new Date();
			glossario.autor = Usuario.find.all().get(0);
			glossario.capitulos = new ArrayList<>();
			
			glossario.save();
			
			Logger.info("Cadastrando o Glossário do Sistema.");
		}
	}
}
