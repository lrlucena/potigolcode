package forms;

import java.security.NoSuchAlgorithmException;

import models.Usuario;

public class LoginForm {
	public String login;
	public String senha;
	
	public String validate() throws NoSuchAlgorithmException {
        if (Usuario.autenticar(login, senha) == null) {
        	return "Login ou Senha Inválida(s). Tente novamente!";
        }
        
        return null;
    }
}
