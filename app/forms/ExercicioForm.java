package forms;

import play.data.validation.Constraints.Required;

public class ExercicioForm {
	@Required(message="O campo deve ser preenchido.")
	public String resposta;
}
