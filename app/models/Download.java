package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.URL;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Download extends Model {
	@Id
	public int id;

	@Required(message = "O campo deve ser preenchido.")
	public String nome;

	@Required(message = "O campo deve ser preenchido.")
	@URL(message = "Você deve informar uma URL válida.")
	public String url;

	@ManyToOne
	public CategoriaDownload categoria;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public CategoriaDownload getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaDownload categoria) {
		this.categoria = categoria;
	}

	public static Finder<Long, Download> find = new Finder<Long, Download>(
			Long.class, Download.class);

	public static List<Download> todos() {
		return find.order("nome").findList();
	}

	public static List<Download> categoria(Long idCategoria) {
		return find.where().eq("categoria_id", idCategoria).findList();
	}
}
