package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Glossario extends Model {
	@Id
	public Long id;
	
	@Required(message="O campo deve ser preenchido.")
	@Column(unique=true)
	public String titulo;

	@Lob
	@Basic(fetch=FetchType.EAGER)
	@Required(message="O campo deve ser preenchido.")
	public String descricao;
	
	@Temporal(TemporalType.DATE)
	public Date dataCadastro;
	
	@OneToOne
	public Usuario autor;
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<CapituloGlossario> capitulos;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataCadastro() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dataCadastro);
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario autor) {
		this.autor = autor;
	}

	public List<CapituloGlossario> getCapitulos() {
		return capitulos;
	}

	public void setCapitulos(List<CapituloGlossario> capitulos) {
		this.capitulos = capitulos;
	}

	public static Finder<Long, Glossario> find = new Finder<Long, Glossario>(Long.class, Glossario.class);
}