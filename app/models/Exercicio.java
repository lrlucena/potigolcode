package models;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import play.data.validation.Constraints.*;
import play.db.ebean.Model;

/**
 * 
 * @author allysonbarros
 */
@Entity
public class Exercicio extends Model {
	@Id
	public Long id;
	
	@Required(message="O campo deve ser preenchido.")
	public String titulo;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	@Required(message="O campo deve ser preenchido.")
	public String descricao;
		
	@Min(1)
	@Max(10)
	@Required(message="O campo deve ser preenchido.")
	public int nivelExercicio;
	
	@Min(100)
	@Max(1000)
	@Required(message="O campo deve ser preenchido.")
	public int pontuacao;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	@Required(message="O campo deve ser preenchido.")
	public String estruturaMetodoProposto;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	@Required(message="O campo deve ser preenchido.")
	public String solucaoProposta;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	@Required(message="O campo deve ser preenchido.")
	public String casoDeTeste;
	
	@ManyToOne
	public Curso curso;

	@OneToOne
	public Usuario autor;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSolucaoProposta() {
		return solucaoProposta;
	}

	public void setSolucaoProposta(String solucaoProposta) {
		this.solucaoProposta = solucaoProposta;
	}

	public int getNivelExercicio() {
		return nivelExercicio;
	}

	public void setNivelExercicio(int nivelExercicio) {
		this.nivelExercicio = nivelExercicio;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public String getCasoDeTeste() {
		return casoDeTeste;
	}

	public void setCasoDeTeste(String casoDeTeste) {
		this.casoDeTeste = casoDeTeste;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario autor) {
		this.autor = autor;
	}
	
	public String getEstruturaMetodoProposto() {
		return estruturaMetodoProposto;
	}

	public void setEstruturaMetodoProposto(String estruturaMetodoProposto) {
		this.estruturaMetodoProposto = estruturaMetodoProposto;
	}

	public static Finder<Long, Exercicio> find = new Finder<>(Long.class, Exercicio.class);
}
