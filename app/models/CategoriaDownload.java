package models;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class CategoriaDownload extends Model {
	@Id
	public int id;

	@Required(message = "O campo deve ser preenchido.")
	@Column(unique = true)
	public String nome;

	@Lob
	@Basic(fetch = FetchType.EAGER)
	@Required(message = "O campo deve ser preenchido.")
	public String descricao;

	@OneToMany(mappedBy = "categoria")
	public List<Download> downloads;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Download> getDownloads() {
		return downloads;
	}

	public void setDownloads(List<Download> downloads) {
		this.downloads = downloads;
	}

	public static Finder<Long, CategoriaDownload> find = new Finder<Long, CategoriaDownload>(
			Long.class, CategoriaDownload.class);

	public static List<CategoriaDownload> todos() {
		return find.order("nome").findList();
	}
}
