package models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * Entidade responsável pela representação dos cursos. <br/>
 * Ex: Fundamentos do Potigol, Programação Orientada à Objetos com Potigol
 * 
 * @author allysonbarros
 */
@Entity
public class Curso extends Model {
	@Id
	public Long id;
	
	@Column(unique=true)
	@Required(message="O campo deve ser preenchido.")
	public String titulo;
	
	@Temporal(TemporalType.DATE)
	public Date	dataCadastro;
	
	@OneToOne(fetch=FetchType.EAGER)
	public Usuario autor;
	
	@OneToMany
	public List<Exercicio> exercicios;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario autor) {
		this.autor = autor;
	}

	public List<Exercicio> getExercicios() {
		return exercicios;
	}

	public void setExercicios(List<Exercicio> exercicios) {
		this.exercicios = exercicios;
	}

	public String getDataCadastro() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dataCadastro);
	}
	
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public static Finder<Long, Curso> find = new Finder<Long, Curso>(Long.class, Curso.class);
	
}
