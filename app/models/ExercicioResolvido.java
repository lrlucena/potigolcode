package models;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class ExercicioResolvido extends Model {
	@Id
	public int id;
	public int pontuacaoObtida;
	
	@Lob
	@Basic(fetch=FetchType.EAGER)
	public String respostaSubmetida;
	
	@ManyToOne
	public Usuario usuario;
	
	@ManyToOne
	public Exercicio exercicio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPontuacaoObtida() {
		return pontuacaoObtida;
	}

	public void setPontuacaoObtida(int pontuacaoObtida) {
		this.pontuacaoObtida = pontuacaoObtida;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}
	
	public String getRespostaSubmetida() {
		return respostaSubmetida;
	}

	public void setRespostaSubmetida(String respostaSubmetida) {
		this.respostaSubmetida = respostaSubmetida;
	}

	public static Finder<Long, ExercicioResolvido> find = new Finder<Long, ExercicioResolvido>(Long.class, ExercicioResolvido.class);
}
