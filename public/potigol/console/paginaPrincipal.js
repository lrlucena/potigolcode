/*
 * PotigolCode - Ambiente de Desenvolvimento para a linguagem Potigol
 * (c) Copyright 2011 - 2012 - DIATINF - Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte 
 * 
 * @@Nome do Curso: Introdução à Programação
 * @@Nome do Professor Autor: Allyson Barros
 */

var licaoAnterior = licaoAtual - 1;
var licaoAtual = 0;
var licaoProxima = licaoAtual + 1;

var licoes = new Array();
var resultadosEsperados = new Array();

// Definição das Lições

licoes.push("Olá! Vamos conhecer uns aos outros. Qual é seu nome?\n" +
		"Digite-o com aspas em torno dele como este \"José\" e pressione ENTER.");

licoes.push("Bom Trabalho! Quantas letras possui o seu nome? " +
		"Descubra digitando o seu nome entre aspas seguido de .comprimento e pressione ENTER.\n" +
		"Exemplo: \"José\".comprimento");